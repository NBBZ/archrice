alias s='sudo'
alias sx='startx ~/.config/x11/xinitrc'
alias cl='clear'
alias xr='xrandr'
alias p='paru'
alias g='git'

alias vim='nvim'
alias ivm='nvim'
alias v='nvim'
alias svim='sudoedit'
alias sv='sudoedit'
#alias fv="du -a ~/.config ~/archrice | awk '{print \$2}' | fzf | xargs -r nvim"
alias fv="du -a ~/archrice | awk '{print \$2}' | fzf | xargs -r nvim"
alias fd="du -a . | awk '{print \$2}' | fzf | xargs -r nvim"
alias pkc='echo Packages: $(paru -Q | wc -l)'

alias ls='lsd --long --versionsort'
alias lt='lsd --all --tree --versionsort'
alias la='lsd'

alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

alias du='du -h'
alias df='df -h'
alias mkdir='mkdir -pv'

alias sd='shutdown now'
alias rb='reboot'
alias py='python3'
alias zz='z ..'
alias clp='colorpicker'
alias speedtest='librespeed-cli'
alias ex='extract'
alias mountphone='simple-mtpfs --device 1 Mount/PHONE'
alias zat='devour zathura'
alias mpv='devour mpv'
alias sxiv='devour nsxiv'
alias pinta='devour pinta'
alias wget='wget --hsts-file="$XDG_DATA_HOME/wget-hsts"'

#vim:foldmethod=marker
