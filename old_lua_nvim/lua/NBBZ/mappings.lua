local map = vim.api.nvim_set_keymap
local opts = {noremap = true, silent = true}

vim.g.mapleader = ' '

map('n', '<F3>', '<Cmd>noh<CR>', opts)             -- Clear search highlight
-- "Enter" key makes a new line without entering insert mode
map('n', '<S-Enter>', '0<Esc>', opts)
map('n', '<CR>', 'o<Esc>', opts)
map('i', '<C-r>', '<Cmd>set revins!<CR>', opts)    -- Reverse insert for RtL langs
map('n', '<leader>r', '<Cmd>set rightleft!<CR>', opts)    -- Reverse insert for RtL langs
map('n', '<leader>c', '<Cmd>w | !gcc -o c_prog % && ./c_prog<CR>', opts) -- Compile and run c
map('n', '<C-d>', '<C-d>zz', opts) -- going down half-screen also centers cursor
map('n', '<C-u>', '<C-u>zz', opts) -- going up half-screen also centers cursor

-- Toggle NvimTree
map('n', '<leader>e', '<Cmd>NvimTreeToggle<CR>', opts)

-- Telescope mappings
--map('n', '<leader>s', '<Cmd>Telescope find_files<CR>', opts) -- Telescope
--map('n', '<leader>f', '<Cmd>Telescope git_files<CR>', opts)  -- Telescope git files
--map('n', '<leader>d', '<Cmd>cd $HOME/archrice <CR>| <Cmd>Telescope find_files<CR>', opts) -- Telescope in dotfiles
--map('n', '<leader>g', '<Cmd>Telescope live_grep<CR>', opts)  -- Telescope live grep

-- Harpoon mappings
--local mark = require('harpoon.mark')
--local ui = require('harpoon.ui')

--vim.keymap.set('n', '<leader>a', mark.add_file)
--vim.keymap.set('n', '<C-e>', ui.toggle_quick_menu)

--vim.keymap.set('n', '<leader>h', function() ui.nav_file(1) end)
--vim.keymap.set('n', '<leader>1', function() ui.nav_file(1) end)
--vim.keymap.set('n', '<leader>2', function() ui.nav_file(2) end)
--vim.keymap.set('n', '<leader>3', function() ui.nav_file(3) end)
--vim.keymap.set('n', '<leader>4', function() ui.nav_file(4) end)
