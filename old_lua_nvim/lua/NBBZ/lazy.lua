--return require'packer'.startup(function()
--    use 'wbthomason/packer.nvim'
--    use 'nvim-tree/nvim-web-devicons' -- Icons
--    use 'EdenEast/nightfox.nvim'      -- Color scheme
--    use 'Mofiqul/dracula.nvim'        -- Second Color scheme
--    use 'nvim-tree/nvim-tree.lua'     -- File manager
--    use 'nvim-lualine/lualine.nvim'   -- Statusline
--    --use 'romgrk/barbar.nvim'        -- Tabs
--    use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'} -- Lots of stuff
--    use 'nvim-lua/plenary.nvim'       -- Dependency
--    use {'nvim-telescope/telescope.nvim', tag = '0.1.0', requires = { {'nvim-lua/plenary.nvim'} }} -- Fuzzy search
--    use 'goolord/alpha-nvim'          -- Srart page
--    use 'norcalli/nvim-colorizer.lua' -- Color preview
--    use 'theprimeagen/harpoon'        -- Cool navigation
--    use 'unblevable/quick-scope'      -- Highlight when using f and t
--
--
--    -- LSP Plugins
--    use 'neovim/nvim-lspconfig'
--    use 'hrsh7th/nvim-cmp'          -- Autocompletion plugin
--    use 'hrsh7th/cmp-nvim-lsp'      -- LSP source for nvim-cmp
--    use 'saadparwaiz1/cmp_luasnip'  -- Snippets source for nvim-cmp
--    use 'L3MON4D3/LuaSnip'          -- Snippets plugin
--    use 'onsails/lspkind.nvim'      -- Completion icons
--end )

require("lazy").setup({

	"wbthomason/packer.nvim",
	"nvim-tree/nvim-web-devicons", -- Icons
	"EdenEast/nightfox.nvim",      -- Color scheme
	"Mofiqul/dracula.nvim",        -- Second Color scheme
	"nvim-tree/nvim-tree.lua",     -- File manager
	"nvim-lualine/lualine.nvim",   -- Statusline
	--"romgrk/barbar.nvim",        -- Tabs
	{"nvim-treesitter/nvim-treesitter", run = ':TSUpdate'}, -- Lots of stuff
	"nvim-lua/plenary.nvim",       -- Dependency
	{"nvim-telescope/telescope.nvim", tag = '0.1.0', requires = { {'nvim-lua/plenary.nvim'} }}, -- Fuzzy search
	--"goolord/alpha-nvim",          -- Srart page
	"norcalli/nvim-colorizer.lua", -- Color preview
	"theprimeagen/harpoon",        -- Cool navigation
	--"unblevable/quick-scope",      -- Highlight when using f and t
	"nvim-treesitter/nvim-treesitter-context",
	
	
	-- LSP Plugins
	"neovim/nvim-lspconfig",
	"hrsh7th/nvim-cmp",          -- Autocompletion plugin
	"hrsh7th/cmp-nvim-lsp",      -- LSP source for nvim-cmp
	"saadparwaiz1/cmp_luasnip",  -- Snippets source for nvim-cmp
	"L3MON4D3/LuaSnip",          -- Snippets plugin
	"onsails/lspkind.nvim",      -- Completion icons

})
