require('nightfox').setup({
  options = {
    -- Compiled file's destination location
    dim_inactive = false,   -- Non focused panes set to alternative background
    styles = {              -- Style to be applied to different syntax groups
      comments = 'italic',    -- Value is any valid attr-list value `:help attr-list`
      conditionals = 'NONE',
      constants = 'NONE',
      functions = 'italic',
      keywords = 'NONE',
      numbers = 'NONE',
      operators = 'NONE',
      strings = 'NONE',
      types = 'NONE',
      variables = 'NONE',
    },
    inverse = {             -- Inverse highlight for different types
      match_paren = false,
      visual = false,
      search = false,
    },

  },
    specs = {
      -- As with palettes, the values defined under `all` will be applied to every style.
      all = {
        syntax = {
          operator = 'cyan.bright',
        },
      },
    },
})

vim.cmd('colorscheme nightfox')
