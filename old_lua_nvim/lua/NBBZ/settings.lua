local set = vim.opt

-- based tabs
set.tabstop = 8
set.shiftwidth = 8
set.softtabstop = 8

set.expandtab = true
set.smarttab = true

set.hlsearch = true
set.incsearch = true
set.ignorecase = true
set.smartcase = true

set.splitbelow = true
set.splitright = true
set.wrap = false
set.title = true
set.scrolloff = 5
set.fileencoding = 'utf-8'
set.termguicolors = true
set.number = true
set.relativenumber = true
set.cursorline = false
set.cursorcolumn = true

set.foldmethod= 'marker'

-- set syntax highlighting for some file types
vim.api.nvim_command([[
augroup CostumeSyntax
autocmd BufNewFile,BufRead *.rasi set ft=css
autocmd BufNewFile,BufRead *aliases set ft=zsh
autocmd BufNewFile,BufRead *.conf set ft=bash
autocmd BufNewFile,BufRead *lfrc set ft=bash
augroup END 
]])
