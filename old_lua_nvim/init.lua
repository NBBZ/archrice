local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require('NBBZ.settings')
require('NBBZ.mappings')
require('NBBZ.lazy')
--require('NBBZ.plugins')      

require('NBBZ.plugins.alpha')      
require('NBBZ.plugins.barbar')     
require('NBBZ.plugins.colorizer')  
require('NBBZ.plugins.harpoon')    
require('NBBZ.plugins.lualine')    
require('NBBZ.plugins.nvim-tree')  
require('NBBZ.plugins.quickscope')
require('NBBZ.plugins.telescope')  
require('NBBZ.plugins.treesitter') 

require('NBBZ.colors.nightfox')
require('NBBZ.language-servers')
require('NBBZ.nvim-cmp')

vim.cmd([[let g:python_recommended_style = 0]])
