-- Space is leader key
vim.g.mapleader = ' '

-- vim file explorer
vim.keymap.set('n', '<leader>pv', vim.cmd.Ex)

-- jump half-screen also centers cursor
vim.keymap.set('n', '<C-d>', '<C-d>zz')
vim.keymap.set('n', '<C-u>', '<C-u>zz')

-- Enter key makes a new line without entering insert mode
vim.keymap.set('n', '<S-Enter>', '0<Esc>')
vim.keymap.set('n', '<CR>', 'o<Esc>')

-- replace current word under cursor
vim.keymap.set('n', '<leader>s', [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
