local set = vim.opt

set.termguicolors = true

set.number = true
set.relativenumber = true

set.expandtab = true
set.smarttab = true

set.smartindent = true

set.wrap = false

-- highlight search
set.hlsearch = true
set.incsearch = true

-- cool behaviour in search
set.ignorecase = true
set.smartcase = true

set.title = true

set.scrolloff = 8

set.cursorline = false
set.cursorcolumn = true

set.updatetime = 50

set.foldmethod= 'marker'
