local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup({
    
        -- Fuzzy finder
    	{ 'nvim-telescope/telescope.nvim', tag = '0.1.4', dependencies = { 'nvim-lua/plenary.nvim' } },
        -- Color scheme
    	{ 'EdenEast/nightfox.nvim' },
        -- It's treesitter
    	{ 'nvim-treesitter/nvim-treesitter', cmd = { 'TSUpdate' } },
	{ 'nvim-treesitter/nvim-treesitter-context' },
        -- Cool navigation
    	{ 'ThePrimeagen/harpoon' },
        -- Statusline
    	{ 'nvim-lualine/lualine.nvim' },
        -- LSP setup
    	{ 'VonHeikemen/lsp-zero.nvim', branch = 'v3.x' },
	{ 'neovim/nvim-lspconfig' },
	{ 'hrsh7th/cmp-nvim-lsp' },
	{ 'hrsh7th/nvim-cmp' },
	{ 'L3MON4D3/LuaSnip' },
        -- Icons
	{ 'nvim-tree/nvim-web-devicons' },
        -- Preview colors
	{ 'norcalli/nvim-colorizer.lua' },

})
