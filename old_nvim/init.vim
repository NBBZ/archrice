"      ___                                       ___     
"     /\  \         _____         _____         /\__\    
"     \:\  \       /::\  \       /::\  \       /::|  |   
"      \:\  \     /:/\:\  \     /:/\:\  \     /:/:|  |   
"  _____\:\  \   /:/ /::\__\   /:/ /::\__\   /:/|:|  |__ 
" /::::::::\__\ /:/_/:/\:|__| /:/_/:/\:|__| /:/ |:| /\__\
" \:\~~\~~\/__/ \:\/:/ /:/  / \:\/:/ /:/  / \/__|:|/:/  / nvim config
"  \:\  \        \::/_/:/  /   \::/_/:/  /      |:/:/  / 
"   \:\  \        \:\/:/  /     \:\/:/  /       |::/  /  
"    \:\__\        \::/  /       \::/  /        |:/  /   
"     \/__/         \/__/         \/__/         |/__/    

" Plugins
call plug#begin()
Plug 'ervandew/supertab'
Plug 'arcticicestudio/nord-vim'
Plug 'phanviet/vim-monokai-pro'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
call plug#end()

" Enable syntax highlighting
syntax enable

" True color
set termguicolors

" Colorscheme
" Blackspace
"let g:blackspace_italics=1
"colorscheme blackspace

" Solarized
" set background=dark
" colorscheme solarized
" highlight Comment cterm=italic

" Monokai Pro
colorscheme monokai_pro

" Smart case search
set ignorecase
set smartcase

" Latex preview viewer
let g:livepreview_previewer = 'zathura'

set nocompatible

" Line numbers
set nu

" Add mouse funcionality
set mouse=a

" Highlight current row and column
set cursorline
set cursorcolumn

" New line without insert mode with enter
nmap <S-Enter> 0<Esc>
nmap <CR> o<Esc>

" Clear highlighting for search with F3
nmap <F3> :noh<CR>

" Fold method
set foldmethod=marker

" Add window title
set title

" Change search match colors
hi Search guibg=#FFD866 guifg=black

" Syntax highlighting for different files
autocmd BufNewFile,BufRead *.rasi set ft=css
autocmd BufNewFile,BufRead *aliases set ft=zsh
autocmd BufNewFile,BufRead *.conf set ft=sh
autocmd BufNewFile,BufRead *lfrc set ft=bash
