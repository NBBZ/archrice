#!/usr/bin/env bash
DEST=$HOME/archrice

mkdir -p $HOME/.config 2>&1 > /dev/null

link(){
	ln -s $DEST/$1 $HOME/.config/ 
}

config_files=("alacritty" "qtile" "rofi" "picom" "spectrwm" "mpv" "moc" "lf" "lsd" "zathura" "cava" "dunst" "qutebrowser" "x11" "zsh" "spicetify" "nvim" "BetterDiscord")

for file in ${config_files[@]}
do
	link $file
done

echo 'remember to set ZDOTDIR in /etc/zsh/zshenv!!!!'
echo 'export ZDOTDIR="$HOME"/.config/zsh'
