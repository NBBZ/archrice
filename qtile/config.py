﻿#      ___                                       ___
#     /\  \         _____         _____         /\__\
#     \:\  \       /::\  \       /::\  \       /::|  |
#      \:\  \     /:/\:\  \     /:/\:\  \     /:/:|  |
#  _____\:\  \   /:/ /::\__\   /:/ /::\__\   /:/|:|  |__ 
# /::::::::\__\ /:/_/:/\:|__| /:/_/:/\:|__| /:/ |:| /\__\
# \:\~~\~~\/__/ \:\/:/ /:/  / \:\/:/ /:/  / \/__|:|/:/  / qtile config
#  \:\  \        \::/_/:/  /   \::/_/:/  /      |:/:/  /
#   \:\  \        \:\/:/  /     \:\/:/  /       |::/  /
#    \:\__\        \::/  /       \::/  /        |:/  /
#     \/__/         \/__/         \/__/         |/__/

# THIS CONFIG HAS A COUPLE OF CHANGES, THE GROUPS BEHAVE LIKE THE WORKSPACES IN i3 (automatically hide when not in use)
# AND THE LAYOUTS HAVE MODIFIED BINDINGS

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = 'mod4'
alt = 'mod1'
terminal = guess_terminal()

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = True
follow_mouse_focus = True
auto_fullscreen = True
focus_on_window_activation = 'smart'
reconfigure_screens = True
auto_minimize = False

# {{{ Keybindings
keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    Key([mod], 'h', lazy.layout.shrink_main(), desc='Shrink master window'),
    Key([mod], 'l', lazy.layout.grow_main(), desc='Grow master window'),
    Key([mod], 'j', lazy.layout.down(), desc='Move focus down'),
    Key([mod], 'k', lazy.layout.up(), desc='Move focus up'),
    Key([mod, 'shift'], 'space', lazy.window.toggle_floating(), desc='Toggle floating on focused window'),
    Key([mod], 'f', lazy.window.toggle_fullscreen(), desc='Toggle fullscreen on focused window'),
    Key([mod, 'shift'], 'h', lazy.layout.swap_left(), desc='Move window to the left'),
    Key([mod, 'shift'], 'l', lazy.layout.swap_right(), desc='Move window to the right'),
    Key([mod, 'shift'], 'j', lazy.layout.shuffle_down(), desc='Move window down in the stack'),
    Key([mod, 'shift'], 'k', lazy.layout.shuffle_up(), desc='Move window up in the stack'),
    Key([mod, 'control'], 'h', lazy.layout.shrink(), desc='Shrink window'),
    Key([mod, 'control'], 'l', lazy.layout.grow(), desc='Grow window'),
    Key([mod, 'control'], 'j', lazy.layout.shrink(), desc='Shrink window'),
    Key([mod, 'control'], 'k', lazy.layout.grow(), desc='Grow window'),
    Key([mod], 'n', lazy.layout.normalize(), desc='Reset all window sizes'),
    Key([mod], 'b', lazy.hide_show_bar(), desc='Toggle bar'),
    Key([mod], 'e', lazy.spawn(terminal), desc='Launch terminal'),
    Key([mod], 'r', lazy.next_layout(), desc='Toggle between layouts'),
    Key([mod, 'shift'], 'q', lazy.window.kill(), desc='Kill focused window'),
    Key([mod, 'shift'], 'r', lazy.reload_config(), desc='Reload the config'),
    Key([mod, 'control'], 'q', lazy.shutdown(), desc='Shutdown Qtile'),
    Key([mod], 'd', lazy.spawn('rofi -show drun'), desc='Open the rofi launcher'),
    Key([mod], 'v', lazy.spawn('alacritty -e pulsemixer'), desc='Open volume mixer'),
    Key([mod, 'shift'], 'd', lazy.spawn('discord'), desc='Open discord'),
    Key([mod, 'shift'], 's', lazy.spawn('spotify'), desc='Open spotify'),
    Key([mod], 'g', lazy.spawn('pcmanfm'), desc='Open pcmanfm'),
    #Key([mod], 't', lazy.spawn('qutebrowser'), desc='Open qutebrowser'),
    #Key([mod], 't', lazy.spawn('brave'), desc='Open brave'),
    Key([mod], 't', lazy.spawn('chromium'), desc='Open Browser'),
    Key([mod], 'space', lazy.widget['keyboardlayout'].next_keyboard(), desc='Next keyboard layout'),
    Key([], 'XF86AudioRaiseVolume', lazy.spawn('pulsemixer --change-volume +10')),
    Key([], 'XF86AudioLowerVolume', lazy.spawn('pulsemixer --change-volume -10')),
    Key([], 'XF86AudioMute', lazy.spawn('pulsemixer --toggle-mute')),
    Key([], 'XF86AudioPlay', lazy.spawn('playerctl play-pause')),
    Key([], 'XF86AudioNext', lazy.spawn('playerctl next')),
    Key([], 'XF86AudioPrev', lazy.spawn('playerctl previous')),
    #Key(['shift'], 'Print', lazy.spawn('maim ~/Pictures/Screenshots/$(date).png')),
]
#}}}

# {{{ Colors

# Carbonfox
colors = dict(
    black =  '#282828',
    red =    '#ee5396',
    green =  '#25be6a',
    yellow = '#dbc074',
    blue =   '#33b1ff',
    purple = '#be95ff',
    cyan =   '#08bdba',
    white =  '#e4e4e5',
    gray =   '#484848',
    background = '#161616',
    foreground = '#f2f4f8',
)

# Nightfox
#colors = dict(
#    black =  '#393b44',
#    red =    '#c94f6d',
#    green =  '#81b29a',
#    yellow = '#dbc074',
#    blue =   '#719cd6',
#    purple = '#9d79d6',
#    cyan =   '#63cdcf',
#    white =  '#dfdfe0',
#    gray =   '#575860',
#    background = '#192330',
#    foreground = '#cdcecf',
#)

# Dracula
#colors = dict(
#    black =  '#44475a',
#    red =    '#ff5555',
#    green =  '#50fa7b',
#    yellow = '#f1fa8c',
#    blue =   '#bd93f9',
#    purple = '#ff79c6',
#    cyan =   '#8be9fd',
#    white =  '#f8f8f2',
#    gray =   '#6272a4',
#    orange = '#ffb86c',
#    background = '#282a36',
#    foreground = '#f8f8f2',
#)

# Monokai pro with orange changed to blue
#colors = dict(
#    white = '#fcfcfa',
#    red = '#ff6188',
#    green = '#a9dc76',
#    yellow = '#ffd866',
#    blue = '#5dacff',
#    purple = '#ab9df2',
#    cyan = '#78dce8',
#    gray = '#423f42',
#    black = '#2d2a2e',
#    background = '#2d2a2e'
#    foreground = '#fcfcfa'
#)
# }}}

# {{{ Screens/ Bars/ Widgets
widget_defaults = dict(
    font ='mononoki Nerd Font',
    #font ='TerminessTTF Nerd Font',
    fontsize = 16,
    padding = 5,
    background = colors['background'],
    foreground = colors['foreground'],
)
extension_defaults = widget_defaults.copy()

Mpris2 = widget.Mpris2(
    fmt = '  {}',
    foreground = colors['green'],
    name='spotify',
    objname='org.mpris.MediaPlayer2.spotify',
    display_metadata=['xesam:artist', 'xesam:title'],
)

Df = widget.DF(
    partition = '/home/nbbz/Mount/TBHD',
    format = '  {uf}{m}|{r:.0f}%',
    foreground = colors['green'],
    visible_on_warn = False,
)

Volume = widget.Volume(
    fmt = '  {}',
    foreground = colors['yellow'],
    volume_app = 'pactl',
    check_mute_command = 'pactl get-sink-mute @DEFAULT_SINK@',
    check_mute_string = 'Mute: yes',
    get_volume_command = 'pactl get-sink-volume @DEFAULT_SINK@',
)

KeyboardLayout = widget.KeyboardLayout(
    fmt = '  {}',
    foreground = colors['purple'],
    configured_keyboards = ['us','il'],
)

Date = widget.Clock(
    fmt = '  {}',
    foreground = colors['cyan'],
    format = '%a, %b %d',
)

Clock = widget.Clock(
    fmt = '  {}',
    foreground = colors['red'],
    format = '%H:%M',
)

# Screens/Bar layout {{{
screens = [
    Screen(top = bar.Bar([
                widget.GroupBox(
                    highlight_method='line',
                    highlight_color=[widget_defaults['background']],
                    padding = 3,
                    hide_unused = True,
                    borderwidth = 2,
                    inactive = widget_defaults['foreground'],
                    this_current_screen_border = colors['blue'],
                    this_screen_border = colors['gray'],
                    other_current_screen_border = widget_defaults['background'],
                    other_screen_border = widget_defaults['background'],
                ),
                widget.CurrentScreen(
                    active_text = '',
                    active_color = colors['blue'],
                    inactive_text = '',
                    inactive_color = colors['gray'],
                    fontsize = 15,
                ),
                widget.WindowName(fmt = '({})'),
                Mpris2,
                widget.Spacer(length = bar.STRETCH),
                Df,
                Volume,
                KeyboardLayout,
                Date,
                Clock,
            ],
            22,
        ),
    ),

    Screen(top = bar.Bar([
                widget.GroupBox(
                    highlight_method='line',
                    highlight_color=[widget_defaults['background']],
                    padding = 3,
                    hide_unused = True,
                    borderwidth = 2,
                    inactive = widget_defaults['foreground'],
                    this_current_screen_border = colors['blue'],
                    this_screen_border = colors['gray'],
                    other_current_screen_border = widget_defaults['background'],
                    other_screen_border = widget_defaults['background'],
                ),
                widget.CurrentScreen(
                    active_text = '',
                    active_color = colors['blue'],
                    inactive_text = '',
                    inactive_color = colors['gray'],
                    fontsize = 15,
                ),
                widget.WindowName(fmt = '({})'),
                Mpris2,
                widget.Spacer(length = bar.STRETCH),
                Df,
                Volume,
                KeyboardLayout,
                Date,
                Clock,
            ],
            22,
        ),
    ),
]
# }}}

# }}}

# {{{ Group stuff
#groups = [Group(i) for i in '123456789']
groups = [
    Group('1', label='一'),
    Group('2', label='二'),
    Group('3', label='三'),
    Group('4', label='四'),
    Group('5', label='五'),
    Group('6', label='六'),
    Group('7', label='七'),
    Group('8', label='八'),
    Group('9', label='九'),
    Group('0', label='十'),
]

# i3-like group switching (THANK YOU elParaguayo#6813!)
@lazy.function
def switch_to_group_screen(qtile, groupname: str | None = None):
    # This shouldn't happen...
    if groupname is None:
        return
    group = qtile.groups_map[groupname]
    # if group is on a screen...
    if group.screen:
        qtile.focus_screen(group.screen.index)
    # it's not on a screen
    else:
        group.cmd_toscreen()


for i in groups:
    keys.extend(
        [
          # mod1 + letter of group = switch to group
          Key([mod], i.name, switch_to_group_screen(groupname=i.name), desc='Switch to group {}'.format(i.name),),

          # Default Keybind
          #Key([mod], i.name, lazy.group[i.name].toscreen(), desc='Switch to group {}'.format(i.name),),

          # mod1 + shift + letter of group = move focused window to group
          Key([mod, 'shift'], i.name, lazy.window.togroup(i.name),
              desc='move focused window to group {}'.format(i.name)),
        ]
    )
# }}}

# {{{ Layouts
layouts = [
    layout.MonadTall(
        border_focus = colors['green'],
                     border_normal = colors['gray'],
                     margin = 4,
                     border_width = 2,
                     single_margin = 0,
                     ),

    layout.MonadWide(border_focus = colors['green'],
                     border_normal = colors['gray'],
                     margin = 4,
                     border_width = 2,
                     single_margin = 0,
                     ),
    layout.Max(),
]


# Drag floating layouts.
mouse = [
    Drag([mod], 'Button1', lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], 'Button3', lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], 'Button2', lazy.window.bring_to_front()),
]

# {{{ Floating rules
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class='confirmreset'),  # gitk
        Match(wm_class='makebranch'),  # gitk
        Match(wm_class='maketag'),  # gitk
        Match(title='branchdialog'),  # gitk
        Match(title='pinentry'),  # GPG key password entry
        Match(wm_class='Godot'),  # Godot popups (also the main window but its not intended)
        Match(wm_class='Godot_Engine'),  # Godot popups (also the main window but its not intended)
    ],
    border_focus = colors['green'],
    border_normal = colors['gray'],
    border_width = 2,
)
# }}}

# }}}

# If Spotify opens move it to proper group
@hook.subscribe.client_name_updated
def spotify(window):
    if window.name == 'Spotify':
        window.togroup(group_name='3')

wl_input_rules = None
wmname = 'qtile'
